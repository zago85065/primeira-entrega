Este é um site criado para que eu possa escrever minhas coisas favoritas sobre a série de jogos Final Fantasy, minha série de jogos favorita!

Neste site, você encontrará

- Jogos da série
- Personagens importantes
- Magias

Para acessar esses tópicos ou saber mais sobre o site, use o menu de navegação no topo da página.
