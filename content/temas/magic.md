---
title: "Magias"
date: 2021-10-20T15:24:16-03:00
draft: false
---

São dezenas de magias diferentes para se utilizar no decorrer das aventuras! 


**Firaga**

![alt text](https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQhAIm10tuLpjsoO371dWnR8X8QjOtUv_1iT8Oppt5BX7XFP2-guVb2t-mW2kWs5Qn4sHg&usqp=CAU)

Magia mais poderosa da propriedade fogo. 

**Thundaga**

![alt text](https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRc27UYIi2_MwaxPkSCFSNEdGpRAfMon4LgQQ&usqp=CAU)

Magia mais poderosa da propriedade trovão.

**Blizzaga**

![alt text](https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTv03z2-9qszyjuenwga9g8hjKbdXcSnrizSQ&usqp=CAU)

Magia mais poderosa da propriedade gelo.
