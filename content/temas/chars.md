---
title: "Personagens importantes"
date: 2021-10-14T11:58:32-03:00
draft: false
---

O que seria de uma boa história se não fosse seus personagens? Dentre os 15 jogos da linha principal eu estarei listando abaixo o meu TOP 10 melhores personagens da série!

 10. **Ardyn**

![alt text](https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSJTYn6ppguoQ3jp46-yRLck-E4tVoFSrRuTw&usqp=CAU)

O último vilão lançado na série até a data na qual foi feita este site. Fizeram um bom trabalho em sua criação, na qual o fizeram um sucessor legítimo dos reis e que teve seu lugar usurpado. Condenado a lutar contra a sua vontade pelos deuses, Ardyn tem seu trágico fim na batalha final contra o jogador.

9. **Balthier**

![alt text](https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTKniRHZtdHllm4Qoxxh_l_EGjO3znVc-sYcA&usqp=CAU)

Famoso pirata dos céus do mundo de Ivalice. Balthier decidiu fugir de casa quando seu pai enlouqueceu enquanto desenvolvia armas de destruição em massa, apenas para ter que voltar a confrontá-lo por um acaso sádico do destino. Balthier cresceu como um rapaz empático, mas que gosta de manter as situações sob seu comando, desempenhando o papel de líder.

8. **Cecil**

![alt text](https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS9j4vMdzshwaCxNsHyrvpoZngwJGJvMAKh0A&usqp=CAU)

A história de Cecil conta o clássico arco de redenção de um paladino. Treinado como um cavalheiro das trevas, Cecil se envergonha de suas ações e é banido de seu reino ao questionar os métodos de seu rei. Em sua jornada ele escala o monte sagrado no qual ele lutou contra sua própria versão distorcida e maligna que representava seus pecados e erros, tornando-se ao final um paladino.

7. **Lightning**

![alt text](https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQPTKrKrwQdngV2di1Kvs60OrlRJl8qceazjw&usqp=CAU)

Uma soldada treinada e muito competente. Lightning tem habilidades acrobáticas incríveis, que permitem grandes vantagens durante combates. No decorrer de sua história ela é amaldiçoada por um ser superior a destruir tudo que ela conhecia, sendo que a tarefa deve ser concluída dentro de um limite de tempo. Em caso de falha, ela se tornaria um monstro incapaz de pensar. 

A história de Lightning nos inspira a enfrentar nossos destinos.

6. **Aerith**

![alt text](https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTDdevpNHwyDZ4IeLUsof9iL7YFHtiZ2TIiMA&usqp=CAU)

Aerith era a típica garota bonita e gentil que se vê em várias narrativas de ficção, uma moça doce, gentil e ingênua. Porém, com o remake que fizeram de Final Fantasy VII, Aerith foi retrabalhada para agir de forma mais idenfificável com os jogadores. Ela é uma moça de muito bom humor, dedicada a mudar a história da forma como nós conhecemos. 

5. **Vivi**

![alt text](https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTQIzm_9t0SoVQoE2SlYr2Voj_cIndgptmw5g&usqp=CAU)

Basicamente, Vivi é um criança criada artifialmente para ser usada como uma arma de guerra. Diferente dos demais da sua "espécie", ele é um protótipo criado com consciência e livre arbítrio que se separou de seus criadores antes que pudesse ser descartado. Apesar de ser uma criança, os escritores lhe deram uma história trágica, na qual ele descobre não ter muito tempo de vida depois de seu nascimento e por isso frequentemente o jogador será exposto a seus pensamentos e crises existenciais.

4. **Noctis**

![alt text](https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS1GHfrAQ7tpX0lPsA6oOOBTYCzMlONC_Scvg&usqp=CAU)

Príncipe da mais nova nação em ruínas do planeta, Noctis começa sua história perdendo sua cidade natal e seus entes queridos. Através do apoio de seus três grandes amigos, ele se fortalece para derrubar o império que conquistou seu lar. Devido a seu sangue real, Noctis é capaz de invocar armas e de se teleportar pelos cenários das batalhas. 

3. **Kain**

![alt text](https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSFckUQM0Og5aDkbaJ2AZsH2tIchmy_7jxZ6tgmCfqKTDntphRKlIHjE29ov73pGODVRoM&usqp=CAU)

Kain cresceu sob a sombra de seu melhor amigo. Durante sua vida ele buscou ter destaque e trazer orgulho para seu reino, porém nunca conseguiu superar seu amigo, o qual conquistou o coração da dama pela qual se apaixonou. Kain representa a sensação de não se sentir bom o bastante, de se dedicar e lutar por algo e se sentir derrotado por não conseguir vencer. Tenho certeza que muitos já passaram por isso, mostrando o quão humanos e relacionáveis os personagens de ficção conseguem ser.


2. **Yuna**

![alt text](https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSyQ2mvTfZECHSSyCu6cBpOilCAXavvQ3OZqg&usqp=CAU)

Yuna é uma invocadora determinada a impedir o extermíneo de muitas vidas em um futuro próximo. Ela embarca numa aventura com seus guardiões para conseguir a invocação suprema que retardará o apocalipse mesmo sabendo que custará sua própria vida.

1. **Ashe**

![alt text](https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQbJ6NoedSmujVQDufDwyR-bYnUhFyZn8OCtQ&usqp=CAU)

Diante as adversidades da guerra, a princesa Ashe embarca em uma jornada para conseguir poder. Seus esforços resultam no descobrimento de um fonte de poder capaz de dizimar cidades inteiras em segundos, colocando-a em um dilema ético sobre liberdade, vitória e justiça. 

"Eu sou simplesmente eu mesma. E eu quero apenas ser livre."