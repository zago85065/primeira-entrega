---
title: "Jogos da série"
date: 2021-10-14T11:58:14-03:00
draft: false
---

Sendo uma das maiores franquias de jogos eletrônicos, Final Fantasy tem uma longa história com 15 jogos de sua linha principal sendo lançados desde 1987! Abaixo estarão expostos os icônicos logos de cada um dos principais lançamentos.



1. **Final Fantasy I**

![alt text](https://www.finalfantasykingdom.net/1psp/logo.jpg)

- Possui diversas versões;
- Deu início a esta grande franquia;

2. **Final Fantasy II**

![alt text](https://www.finalfantasykingdom.net/2psp/logo.jpg)

- Introduziu os famosos chocobos!;
- Foi o primeiro a se arriscar a traçar narrativas mais elaboradas;

3. **Final Fantasy III**

![alt text](https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ5hGDCgTkiI_Z-qe9s-qHEplwWpVCCC7Ko48bBHfKobuHAO2c4HEojSNL8xQyCxEe_ccE&usqp=CAU)

- Introduziu o glorioso sistema de clases;
- Considerado um dos mais difíceis da série;

4. **Final Fantasy IV**

![alt text](https://www.finalfantasykingdom.net/4psp/ff4logo.png)

- O favorito de muitos fãs, fazendo muitos fãs novos com sua história bem desenvolvida e músicas cativantes;

5. **Final Fantasy V**

![alt text](https://www.finalfantasykingdom.net/5/logo.jpg)

- Trouxe a melhor versão do sistema de classes;
- Possui os chefes mais difíceis da série;

6. **Final Fantasy VI**

![alt text](https://www.finalfantasykingdom.net/6/logo2.jpg)

- Discutivelmente o melhor de todos pela sua história, personagens, músicas e sistema de combate;
- Aclamado como um dos melhores jogos de todos os tempos;

7. **Final Fantasy VII**

![alt text](https://www.finalfantasykingdom.net/7/logo.jpg)

- O mais popular de toda a série;
- Aclamado como um dos melhores jogos de todos os tempos;

8. **Final Fantasy VIII**

![alt text](https://www.finalfantasykingdom.net/8/logo.jpg)

- Um dos mais controversos, devido a seu sistema de magias;
- Grande polarizador entre os fãs;

9. **Final Fantasy IX**

![alt text](https://www.finalfantasykingdom.net/9/logo.jpg)

- Um dos mais amados entre os fãs;
- Considerado o favorito do próprio criador da franquia;

10. **Final Fantasy X**

![alt text](https://www.finalfantasykingdom.net/10/ffxlogo.gif)

- Considerado um dos jogos com melhores gráficos do Playstation 2;
- Aclamado como um dos melhores jogos de todos os tempos;
- Possui centenas de horas de conteúdo extra para os jogadores;

11. **Final Fantasy XI**

![alt text](https://www.finalfantasykingdom.net/11/ffxilogo.gif)

- O primeiro jogo da série feito na forma de multijogador massivo online (ou Massive Multiplayer Online - MMO);
- Pessoas de diversos lugares podem se conectar e fazerem suas próprias aventuras em um mundo online;

12. **Final Fantasy XII**

![alt text](https://www.finalfantasykingdom.net/12/logo.gif)

- Possui um sistema de combate único;
- Um dos menos jogados pela comunidade de fãs;

13. **Final Fantasy XIII**

![alt text](https://www.finalfantasykingdom.net/13/ffxiiilogo.gif)

- Um dos mais controversos, devido a forma com a qual sua história é contada;
- Tem ótimos gráficos para o Playstation 3; 

14. **Final Fantasy XIV**

![alt text](https://www.finalfantasykingdom.net/xiv/xivlogo.gif)

- Segundo jogo online de modo multijogador massivo da série;

15. **Final Fantasy XV**

![alt text](https://www.finalfantasykingdom.net/15/15logo1.png)

- Primeiro jogo no estilo de "mundo aberto" da série;
- Criticado por ter elementos importantes da história fechados através de conteúdos de download adicionais pagos; 
